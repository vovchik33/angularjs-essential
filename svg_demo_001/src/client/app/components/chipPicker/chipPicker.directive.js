module.exports = function(ngApp) {
    ngApp.directive('chipPicker', function(){
        return {
            scope: false,
            controller: 'chipPickerCtrl',
            controllerAs: 'chipPickerCtrl',
            template: require('./chipPicker.template.html'),
            link: function(scope, elem, attrs) {

            }
        }
    });
}