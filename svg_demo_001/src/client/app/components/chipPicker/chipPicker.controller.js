module.exports = function(ngApp) {
    ngApp.controller('chipPickerCtrl', ['$scope', function($scope){
        var self = this;
        this.chips = [
            {value:1, color:"blue", active:true},
            {value:5, color:"red"},
            {value:10, color:"yellow"},
            {value:25, color:"navy"},
            {value:50, color:"brown"}
        ];
        this.chipIndex=0;
        this.onClickHandler = function(value) {
            self.chips[self.chipIndex].active = false;
            self.chipIndex = value;
            self.chips[self.chipIndex].active = true;
        }
        this.getChipValue = function () {
            return self.chips[self.chipIndex].value;
        }
        this.getChipColor = function () {
            return self.chips[self.chipIndex].color;
        
        }
    }]);
}