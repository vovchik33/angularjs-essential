var angular = require('angular');
var app = require('./app')('ngApp', angular);

require('./components/chipPicker/chipPicker.directive')(app);
require('./components/chipPicker/chipPicker.controller')(app);
require('./components/chipPicker/chipPicker.less');