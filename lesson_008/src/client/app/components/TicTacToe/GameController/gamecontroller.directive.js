module.exports = function(ngApp) {
    ngApp.directive('gameController',['tictactoeService', function(tictactoeService) {
        return {
            controller: ['$scope','tictactoeService',function($scope, tictactoeService) {
                $scope.statusDisabled = true;
                $scope.$watch(tictactoeService.getWinnerId, function(newValue, oldValue) {
                    $scope.statusDisabled = (newValue==0);
                });
                $scope.clickRestartHandler = function() {
                    tictactoeService.restart();
                }
            }],
            template: "<div><input type='button' ng-disabled='statusDisabled' value='Restart' ng-click='clickRestartHandler()'></input></div>",
            link: function(scope, elem, attrs) {

            }
        }
    }])
}