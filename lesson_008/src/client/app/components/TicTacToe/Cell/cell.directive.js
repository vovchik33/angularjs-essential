module.exports = function(ngApp) {
    ngApp.directive('cell', function() {
        return {
            scope: {
                value: "@"
            },
            controller: 'cellCtrl',
            template: require('./cell.template.html'),
            link: function(scope, elem, attrs) {
                scope.value = attrs.value;
                scope.col = attrs.col;
                scope.row = attrs.row;
            }
        }
    })
}