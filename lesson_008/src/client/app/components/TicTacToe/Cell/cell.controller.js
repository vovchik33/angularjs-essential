module.exports = function(ngApp) {
    ngApp.controller('cellCtrl', ['$scope', 'tictactoeService',function($scope, tictactoeService) {
        $scope.value = 0;
        $scope.col = 0;
        $scope.row = 0;
        $scope.clickCellHandler = function () {
            tictactoeService.turn($scope.col,$scope.row);
        }
    }])
}