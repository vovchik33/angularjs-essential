module.exports = function (angular, appName) {
    var app = angular.module(appName, []);

    require('../services/TicTacToe.service')(app);
    
    require('../GameTable/gametable.directive')(app);
    require('../GameTable/gametable.controller')(app);
    require('../GameTable/gametable.less');

    require('../Cell/cell.directive')(app);
    require('../Cell/cell.controller')(app);
    require('../Cell/cell.less');

    require('../GameController/gamecontroller.directive')(app);
} 