module.exports = function(ngApp) {
    ngApp.service('tictactoeService', function($interval) {
        var currentTurn = 0;
        var gameState = 0;

        var tableState = [
            {index:0, values:[0,0,0]},
            {index:1, values:[0,0,0]},
            {index:2, values:[0,0,0]}
        ];

        var players = [
            "unknown",
            "player #X",
            "player #O"
        ];

        function restart() {
            tableState = [
                {index:0, values:[0,0,0]},
                {index:1, values:[0,0,0]},
                {index:2, values:[0,0,0]}
            ];
            gameState = 0;
        }

        function turn(col, row) {
            if (tableState[row].values[col]==0 && gameState==0) {
                tableState[row].values[col] = currentTurn+1;
                currentTurn=(currentTurn+1)%2;
                gameState=checkForWinners();
            }
        }

        function checkForWinners() {
            if (tableState[0].values[0]==tableState[0].values[1] && tableState[0].values[1]==tableState[0].values[2] && tableState[0].values[0]>0) return tableState[0].values[0];
            if (tableState[1].values[0]==tableState[1].values[1] && tableState[1].values[1]==tableState[1].values[2] && tableState[1].values[0]>0) return tableState[1].values[0];
            if (tableState[2].values[0]==tableState[2].values[1] && tableState[2].values[1]==tableState[2].values[2] && tableState[2].values[0]>0) return tableState[2].values[0];
            
            if (tableState[0].values[0]==tableState[1].values[0] && tableState[1].values[0]==tableState[2].values[0] && tableState[2].values[0]>0) return tableState[2].values[0];
            if (tableState[0].values[1]==tableState[1].values[1] && tableState[1].values[1]==tableState[2].values[1] && tableState[2].values[1]>0) return tableState[2].values[1];
            if (tableState[0].values[2]==tableState[1].values[2] && tableState[1].values[2]==tableState[2].values[2] && tableState[2].values[2]>0) return tableState[2].values[2];

            if (tableState[0].values[0]==tableState[1].values[1] && tableState[1].values[1]==tableState[2].values[2] && tableState[1].values[1]>0) return tableState[1].values[1];
            if (tableState[0].values[2]==tableState[1].values[1] && tableState[1].values[1]==tableState[2].values[0] && tableState[1].values[1]>0) return tableState[1].values[1];
            
            return 0;
        }

        return {
            getGreeting: function getGreeting() {
                return "Winner is ";            
            },
            getTableState: function() {
                return tableState;
            },
            getCurrentTurn: function() {
                return currentTurn;
            },
            getGameState: function() {
                return gameState;
            },
            getWinner: function() {
                return players[gameState];
            },
            getWinnerId: function() {
                return gameState;
            },
            turn: turn,
            restart: restart
        }
    })
}