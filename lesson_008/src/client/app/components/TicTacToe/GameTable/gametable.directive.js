module.exports = function (ngApp) {
    ngApp.directive('gameTable', function() {
        return {
            template: require('./gametable.template.html'),
            controller: 'gameTableCtrl',
            link: function (scope, elem, attrs) {
                
            }
        }
    })
}