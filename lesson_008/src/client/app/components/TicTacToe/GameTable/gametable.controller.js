module.exports = function(ngApp) {
    ngApp.controller('gameTableCtrl', ['$scope','tictactoeService', function($scope,tictactoeService) {
        $scope.winner=tictactoeService.getWinner();

        $scope.$watch(tictactoeService.getGreeting, function(newValue, oldValue) {
            $scope.greeting = newValue;
        })

        $scope.$watch(tictactoeService.getWinner, function(newValue, oldValue) {
            $scope.winner = newValue;
        })

        $scope.$watch(tictactoeService.getTableState, function(newValue, oldValue) {
            $scope.tableState = newValue;
        })
        
        $scope.$watch(tictactoeService.getGameState, function(newValue, oldValue) {
            $scope.gameState = newValue;
        })
    }])
}