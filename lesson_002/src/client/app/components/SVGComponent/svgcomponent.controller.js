module.exports = function(ngApp) {
    ngApp.controller('svgComponent', function($scope){
        $scope.version="0.0.0";
        $scope.position={
            top:100,
            left:200
        }
        this.getTop=function(){
            return $scope.position.top;
        }
        this.getLeft=function(){
            return $scope.position.left;
        }
    })
}