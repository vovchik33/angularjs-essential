module.exports = function(ngApp) {
    ngApp.directive('svgComponent', function() {
        return {
            scope: {
                name: '='
            },
            controller: 'svgComponent',
            controllerAs: 'ctrl',
            template: require('./svgcomponent.template.html'),
            link: function(scope, elem, attrs,ctrl) {
                scope.version = attrs.version;
                
            }
        }
    })
} 