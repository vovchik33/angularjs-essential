var angular = require('angular');
var app = angular.module('appMain', []);

require('./components/SimpleComponent/simplecomponent.directive')(app);
require('./components/SimpleComponent/simplecomponent.controller')(app);

require('./components/SVGComponent/svgcomponent.less');
require('./components/SVGComponent/svgcomponent.directive')(app);
require('./components/SVGComponent/svgcomponent.controller')(app);
