var angular = require('angular');
var app = angular.module('appMain', []);

require('./components/SimpleComponent/simplecomponent.directive')(app);
require('./components/SimpleComponent/simplecomponent.controller')(app);

require('./components/DoubledString/doubledString.directive')(app);
require('./components/DoubledString/doubledString.controller')(app);
