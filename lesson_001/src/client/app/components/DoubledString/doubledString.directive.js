module.exports = function(ngApp) {
    ngApp.directive('doubledString', function() {
        return {
            controller: 'doubledString',
            template: require('./doubledString.template.html'),
            link: function(scope, element,attrs) {
                scope.src=attrs.src;
            }
        }
    })
}