module.exports = function (ngApp) {
    ngApp.controller('counterComponentController', [
        '$scope',
        '$rootScope',
        'timerService',
        'logService',
        function ($scope, $rootScope, timerService, logService) {
            timerService.startCounter();
            $scope.hello = 0;
            $rootScope.$on('timer-service-event', function () {
                $scope.hello = timerService.getCounter();
                logService.log("ROOTSCOPE:" + timerService.getCounter());
            });
        }]);
    ngApp.directive('counter', [
        '$rootScope',
        function ($rootScope) {
            return {
                scope: {
                    hello: '='
                },
                controller: 'counterComponentController',
                controllerAs: 'ctrl',
                template: '<div>Counter: {{hello}}</div>',
                link: function (scope, elem, attrs, ctrl) {
                    //scope.hello = "asd";
                    //console.log(scope.hello);
                    ctrl.hello = scope.hello
                }
            }
        }
    ])
}