module.exports = function(ngApp) {
    ngApp.directive('simpleComponent',['$rootScope', 'timerService', function($rootScope, timerService){
        console.log("SimpleComponent Directive is created");
        timerService.startCounter();
        return {
            scope:{
                version:"=?version"
            },
            controller: "simpleComponentController",
            template: require('./simpleComponent.template.html'),
            link: function($scope, element, attrs, ctrl) {
                if (!$scope.version) $scope.version="asd";
                function timerHandler(event, data) {
                    $scope.version = "v."+data;
                    $scope.$apply();
                }
                $rootScope.$on('timer-service-event',timerHandler);  
            }
        }
    }]);
}