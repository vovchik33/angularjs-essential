module.exports = function(ngApp) {
    ngApp.directive('cellComponent', function() {
        return {
            scope: false,
            controller: 'cellComponentController',
            template: require('./cellComponent.template.html'),
            link: function(scope, elem, attrs, ctrl) {
                scope.state = attrs.state;
            }
        }
    });
}