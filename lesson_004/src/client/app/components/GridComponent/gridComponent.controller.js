module.exports = function (ngApp) {
    ngApp.controller('gridComponentController', [
            '$scope',
            'gridService',
            'logService',
            function ($scope, gridService, logService) {
                //$scope.width = gridService.gridState[0].length;
                //$scope.height = gridService.gridState.length;
                //$scope.gridState = gridService.getGridState();
                $scope.gridState = [[0]];
                gridService.registerScope($scope);
                $scope.$on('grid-state-changed', function (event) {
                    //$scope.gridState = gridService.getGridState();
                    //ctrl.gridState = gridService.getGridState();
                    //console.log("CONTROLLER");
                    //console.log($scope.gridState);
                });
            }
        ]);
}