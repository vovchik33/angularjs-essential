module.exports = function (ngApp) {
    ngApp.directive('gridComponent', ['gridService', function (gridService) {
        return {
            // scope:{
            //     gridState: "=?gridState"
            // },
            // transclude: true,
            controller: 'gridComponentController',
            template: require('./gridComponent.template.html'),
            link: function (scope, elem, attrs, ctrl) {
                scope.$on('grid-state-changed', function (event) {
                    scope.gridState = gridService.getGridState();
                    //ctrl.gridState = gridService.getGridState();
                });
                //ctrl.gridState=gridService.getGridState();
                scope.gridState = gridService.getGridState();
                gridService.registerScope(scope);
            }
        }
    }
    ]);
}