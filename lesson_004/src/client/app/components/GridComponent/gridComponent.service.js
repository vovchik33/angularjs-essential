module.exports = function(ngApp) {
    ngApp.service('gridService', ['$rootScope', function($rootScope){
        var gridState=[[]
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0],
            // [0,0,0,0,0,0,0]
        ];
        var $scope;
        $rootScope.$on('timer-service-event',function() {
            gridState[0].push(Math.round(Math.random()));
            $scope.$emit('grid-state-changed');
        });
        return {
            registerScope:function(scope){
                $scope=scope;
               
            },
            getGridState: function() {
                return gridState;
            }
        }
    }])
}