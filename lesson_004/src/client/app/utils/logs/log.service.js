module.exports = function(ngApp) {
    ngApp.service('logService', function(){
        function log(message) {
            console.log("Log: "+ message);
        }
        return {
            log: log
        }
    });
}