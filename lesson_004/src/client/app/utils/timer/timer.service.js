module.exports = function (ngApp) {
    ngApp.service('timerService', [
        '$rootScope',
        'logService',
        function ($rootScope, logService) {
            var counter = 10;
            var timerId;
            function startCounter() {
                timerId = setInterval(handleTimeInterval, 1000);
            }
            function stopCounter() {
                clearInterval(timerId);
            }
            function resetCounter() {
                clearInterval(timerId);
                counter = 0;
            }
            function handleTimeInterval() {
                counter++;
                $rootScope.$emit('timer-service-event', counter);
            }
            function getCounter() {
                return counter;
            }
           
            return {
                getCounter: getCounter,
                startCounter: startCounter,
                stopCounter: stopCounter,
                resetCounter: resetCounter
            }
        }])
}