var angular = require('angular');
var app = angular.module('appMain', []);

require('./styles/base.less');

require('./utils/logs/log.service')(app);
require('./utils/timer/timer.service')(app);

require('./components/CounterComponent/counterComponent.directive')(app);

require('./components/SimpleComponent/simpleComponent.controller')(app);
require('./components/SimpleComponent/simpleComponent.directive')(app);

require('./components/CellComponent/cellComponent.directive')(app);
require('./components/CellComponent/cellComponent.controller')(app);
require('./components/CellComponent/cellComponent.less');

require('./components/GridComponent/gridComponent.controller')(app);
require('./components/GridComponent/gridComponent.directive')(app);
require('./components/GridComponent/gridComponent.service')(app);
require('./components/GridComponent/gridComponent.less');



