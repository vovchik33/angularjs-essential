module.exports = function(ngApp) {
    ngApp.service('timerService', function($interval) {
        var counter=0;
        var isActive=false;
        var interval;

        function getCounter() {
            return counter;
        }
        function getStatus() {
            return isActive;
        }
        function startCounter() {
            isActive = true;
            interval=$interval(function() {
                counter++;
            }, 1000);
        }

        function stopCounter() {
            isActive = false;
            $interval.cancel(interval);
        }

        return {
            getCounter: getCounter,
            startCounter: startCounter,
            stopCounter: stopCounter,
            getStatus: getStatus,
            hasActive: isActive,
            isActive: function () {
                return isActive
            }
        }
    });
}