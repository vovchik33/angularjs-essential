var angular = require('angular');
var app = angular.module('ngApp', []);  

require('./services/TimerService/index')(app);

require('./components/Status/status.directive')(app);

require('./components/Timer/timer.controller')(app);
require('./components/Timer/timer.directive')(app);