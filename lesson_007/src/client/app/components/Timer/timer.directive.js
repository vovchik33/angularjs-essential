module.exports = function (ngApp) {
    ngApp.directive('timer', function () {
        return {
            controller: 'timerController',
            template: require('./timer.template.html'),
            link: function (scope, elem, attrs) {
            }
        }
    });
}