module.exports = function (ngApp) {
    ngApp.controller('timerController', ['$scope', 'timerService', function ($scope, timerService) {
        $scope.timer = 0;
        timerService.startCounter();

        $scope.$watch(timerService.getCounter, function(newValue, oldValue){
            if (newValue==10) {
                timerService.stopCounter();
            }
            $scope.timer = newValue;
        })
    }])
}