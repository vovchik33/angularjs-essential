module.exports = function(ngApp) {
    ngApp.directive('status', function() {
        return {
            controller: ['$scope', 'timerService', function($scope, timerService) {
                $scope.isActive = timerService.getStatus();
                $scope.$watch(timerService.getStatus, function(newValue, oldValue) {
                    console.log("Watched 1 for timerService.getStatus() newValue is "+newValue);
                    $scope.isActive = timerService.getStatus();
                });
                $scope.$watch('isActive', function() { // unable to watch for service's variable, so, it should be arrivaed with the function
                    console.log("Watched 2 for timerService.isActive newValue is "+ timerService.getStatus());
                    //$scope.isActive = timerService.getStatus();
                });
                $scope.$watch('timerService.hasActive', function() { // unable to watch for service's variable, so, it should be arrivaed with the function
                    console.log("Watched 3 for timerService.isActive newValue is "+ timerService.getStatus());
                    //$scope.isActive = timerService.getStatus();
                });
                $scope.$watch(timerService.isActive, function() { // unable to watch for service's variable, so, it should be arrivaed with the function
                    console.log("Watched 4 for timerService.isActive newValue is "+ timerService.isActive());
                    //$scope.isActive = timerService.getStatus();
                });
            }],
            template: '<div>status is {{isActive?"active":"non-active"}}</div>',
            link: function(scope, elem, attrs) {

            }
        }
    });
}