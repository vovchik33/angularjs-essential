module.exports = function(ngApp) {
    ngApp.directive('simpleComponent', function(){
        console.log("SimpleComponent Directive is created");
        return {
            controller: 'simpleComponent',
            template: require('./simplecomponent.template.html'),
            link: function(scope, element, attrs) {
                scope.version = attrs.version;
            }
        }
    });
}