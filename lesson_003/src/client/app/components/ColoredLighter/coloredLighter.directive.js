module.exports = function (ngApp) {
    ngApp.directive('coloredLighter', function () {
        return {
            scope: {
                name: '='
            },
            controller: 'coloredLighterController',
            controllerAs: 'ctrlLighter',
            template: require('./coloredLighter.graphic.template.html'),
            link: function(scope, elem,attrs,ctrl) {
                scope.color = attrs.color;
                scope.switchedOn = (attrs.switchedon==='true');
            }
        }
    })
}