var angular = require('angular');
var app = require('./components/Application')('ngApp');

require('./components/Header/header.controller')(app);
require('./components/Header/header.directive')(app);
