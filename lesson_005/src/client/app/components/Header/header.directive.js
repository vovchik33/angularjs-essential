module.exports = function (ngApp) {
    ngApp.directive('header', function () {
        return {
            controller: 'headerCtrl',
            transclude: true,
            scope: {
                info: '@titleInfo',
                title: '@',
                text: "@textValue",
                reverse: '@reverseName'
            },
            template: '<div>Hel {{title}} {{info}} {{text}}</div>',
            link: function (scope, elem, attrs) {
                console.log(scope);
                console.log(attrs.info);
            }
        }
    });
}